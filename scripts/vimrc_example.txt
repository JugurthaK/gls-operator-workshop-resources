set laststatus=2
set statusline=%f
set shiftwidth=4
set tabstop=4
set expandtab
set number
set selection=exclusive
set autoindent
set autoread
set showcmd
set nocompatible
set relativenumber
set nrformats-=octal
set scrolloff=30
color murphy

autocmd FileType yaml,yml setlocal ai ts=2 sw=2 et
